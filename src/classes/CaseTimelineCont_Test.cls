@isTest
private class CaseTimelineCont_Test {

    
    @testSetup static void settingUp(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias='usr', Email='user@example.com', EmailEncodingKey='UTF-8', LastName = 'User', LanguageLocaleKey='en_US', LocaleSidKey = 'en_GB', ProfileId = p.Id, TimeZoneSidKey='Europe/London', Username='testuser@testclass.testing');
        User v = new User(Alias='usr', Email='user@example.com', EmailEncodingKey='UTF-8', LastName = 'Twoser', LanguageLocaleKey='en_US', LocaleSidKey = 'en_GB', ProfileId = p.Id, TimeZoneSidKey='Europe/London', Username='testwoser@testclass.testing');
        insert u;
        insert v;

        Case exampleCase = new Case();
        System.runAs(u){
            insert exampleCase;
            Long timeNow = System.currentTimeMillis();  
            while(System.currentTimeMillis() < timeNow + 2000){/*Do nothing for 2 seconds*/}
            insert new Task(WhatId = exampleCase.Id, ActivityDate = Date.today().addDays(1), Status='Completed');
            insert new Event(WhatId = exampleCase.Id, WhoId = u.Name, Subject='Subject', StartDateTime=Date.today().addDays(2), DurationInMinutes = 0);
            insert new CaseComment(ParentId = exampleCase.Id, CommentBody = 'Comment Body');
        }
        System.runAs(v){
            Long timeNow = System.currentTimeMillis();
            while(System.currentTimeMillis() < timeNow + 2000){/*Do nothing for 2 seconds*/}
            insert new Attachment(ParentId = exampleCase.Id, Name = 'Attachment', Body = Blob.valueOf('Blob'));
        }
    }

    static testMethod void testTable(){
        Case theCase = [SELECT Id, CreatedDate FROM Case];
        CaseTimelineCont cont = new CaseTimelineCont(new ApexPages.StandardController(theCase));
        cont.selectedCaseIds.add(theCase.Id);
        List<CaseTimelineCont.Row> testList = cont.getRowList();
        System.assertEquals(5, testList.size());
        System.assertEquals(theCase.CreatedDate,testList[0].theDate);
        System.assertEquals('Case History',testList[0].type);
        System.assertEquals('User',testList[0].userName);
        System.assertEquals('Case Created',testList[0].subject);
        System.assertEquals('', testList[0].description);
        System.assertEquals('Comment',testList[1].type);
        System.assertEquals('User',testList[1].userName);
        System.assertEquals('Comment Body',testList[1].subject);
        System.assertEquals('',testList[1].description);
        System.assertEquals('Twoser',testList[2].userName);
        System.assertEquals('Attachment',testList[2].type);
        System.assertEquals('Task',testList[3].type);
        System.assertEquals('User',testList[3].userName);
        System.assertEquals('Event',testList[4].type);
        System.assertEquals('Subject',testList[4].subject);
        System.assertEquals('User',testList[4].userName);
    }

    static testMethod void testBack(){
        Case theCase = [SELECT Id, CreatedDate FROM Case];
        PageReference p = Page.CaseTimeline;
        p.getParameters().put('Id',theCase.Id);
        Test.setCurrentPage(p);
        CaseTimelineCont cont = new CaseTimelineCont(new ApexPages.StandardController(theCase));
        PageReference nextPageActual = cont.backToCase();
        PageReference nextPageExpected = new ApexPages.StandardController(theCase).view();
        System.assertEquals(nextPageExpected.getUrl(), nextPageActual.getUrl());
    }
/*
    static testMethod void testExport(){
        Case theCase = [SELECT Id, CreatedDate FROM Case];
        PageReference p = Page.CaseTimeline;
        p.getParameters().put('Id',theCase.Id);
        Test.setCurrentPage(p);
        CaseTimelineCont cont = new CaseTimelineCont(new ApexPages.StandardController(theCase));
        PageReference nextPageActual = cont.export();
        PageReference nextPageExpected = Page.CaseTimelineExcel;
        nextPageExpected.getParameters().put('Id',theCase.Id);
        System.assertEquals(nextPageExpected.getUrl(), nextPageActual.getUrl());       
    }
*/
}