/*----------------------------------------------------------------------------------------------------------------
Author:         Freddie Wright
Company:        Arcus Global Ltd
Date:           2015-10-07
Test Class:     CaseTimelineCont_Test
Purpose:        To display all types of "activity" on a Case in chronological order.
----------------------------------------------------------------------------------------------------------------*/

public class CaseTimelineCont {
    
    public Case theCase;
    public Id caseId;
    public List<Row> rowList;
    public Map<Id,String> relationContacts;
    public List<Case> allCases;
    public List<Id> selectedCaseIds{get;set;}
    public List<CaseWrapper> caseList = new List<CaseWrapper>();

    public CaseTimelineCont(ApexPages.StandardController cont) {
        this.theCase = (Case)cont.getRecord();
        this.theCase = [SELECT Id, Child__c, Parent__c, CreatedDate FROM Case WHERE Id = :theCase.Id];
        this.relationContacts = getRelationContacts();
        this.allCases = [SELECT Id, CaseNumber, Child__r.Name, Parent__r.Name, RecordType.Name, CreatedDate FROM Case WHERE Child__c in :relationContacts.keySet() AND Child__c != null ORDER BY CaseNumber ASC];
        this.selectedCaseIds = new List<Id>();
    }
        
    public Map<Id,String> getRelationContacts(){
        List<arcuseducate__Relationship__c> relationList = [SELECT Id, arcuseducate__From__c, arcuseducate__Role__c FROM arcuseducate__Relationship__c WHERE arcuseducate__To__c = :theCase.Child__c];
        Map<Id,String> relationMap = new Map<Id,String>{theCase.Child__c => '', null => ''};
        for(arcuseducate__Relationship__c r : relationList){
            relationMap.put(r.arcuseducate__From__c,r.arcuseducate__Role__c);
        }
        return relationMap;
    }

    public PageReference getSelected(){
        selectedCaseIds.clear();
        for(CaseWrapper cw : caseList){
            if(cw.selected){
                selectedCaseIds.add(cw.ca.Id);
            }
        }
        return null;
    }

    public List<CaseWrapper> getCases(){
        for(Case c : allCases){
            caseList.add(new CaseWrapper(c, relationContacts));
        }
        return caseList;
    }

    public class CaseWrapper{
        public Case ca{get;set;}
        public Boolean selected{get;set;}
        public String childRelation{get;set;}
        public String parentRelation{get;set;}

        public CaseWrapper(Case c, Map<Id,String> relationContacts){
            ca = c;
            selected = false;
            childRelation = relationContacts.get(c.Child__r.Id);
            parentRelation = relationContacts.get(c.Parent__r.Id);
            if(childRelation==null){childRelation='';}
            if(parentRelation==null){parentRelation='';}
        }
    }

    public List<Row> getRowList(){
        if(selectedCaseIds == null){selectedCaseIds = new List<Id>();}
        Map<Id,Case> caseMap = new Map<Id,Case>([SELECT Id, CaseNumber, Parent__r.Name, Child__r.Name, RecordType.Name FROM Case WHERE Id in:selectedCaseIds]);
        List<Row> listToReturn = new List<Row>();
        List<Task> taskList = [SELECT Id, Owner.Name, ActivityDate, Subject, Description, WhatId FROM Task WHERE WhatId in:selectedCaseIds AND Status = 'Completed' AND ActivityDate != null];
        for(Task t : taskList){
            listToReturn.add(new Row(t.Id, t.ActivityDate, t.Owner.Name, 'Task', t.Subject, t.Description, caseMap.get(t.WhatId)));
        }
        List<Event> eventList = [SELECT Id, Owner.Name, ActivityDateTime, Subject, Description, WhatId FROM Event WHERE WhatId in:selectedCaseIds];
        for(Event e : eventList){
            listToReturn.add(new Row(e.Id, e.ActivityDateTime, e.Owner.Name, 'Event', e.Subject, e.Description, caseMap.get(e.WhatId)));
        }
        List<Attachment> attachmentList = [SELECT Id, Owner.Name, CreatedDate, Name, Description, ParentId FROM Attachment WHERE ParentId in:selectedCaseIds];
        for(Attachment a : attachmentList){
            listToReturn.add(new Row(a.Id, a.CreatedDate, a.Owner.Name, 'Attachment', a.Name, a.Description, caseMap.get(a.ParentId)));
        }
        List<CaseComment> commentList = [SELECT Id, CreatedBy.Name, CreatedDate, CommentBody, ParentId FROM CaseComment WHERE ParentId in:selectedCaseIds AND isDeleted = false];
        for(CaseComment c : commentList){
            listToReturn.add(new Row(c.Id, c.CreatedDate, c.CreatedBy.Name, 'Comment', c.CommentBody, '', caseMap.get(c.ParentId)));
        }
        List<CaseHistory> historyList = [SELECT Id, CreatedBy.Name, CreatedDate, OldValue, NewValue, Field, CaseId FROM CaseHistory WHERE CaseId in:selectedCaseIds AND isDeleted = false];
        Map<String,Schema.SObjectField> fieldsMap = Schema.SObjectType.Case.fields.getMap();
        for(CaseHistory h : historyList){
            try{
                //We don't want the old or new value to be an Id
                Id oldValueId = (Id)h.OldValue;
                Id newValueId = (Id)h.NewValue;
            }catch(Exception e){
                if(h.NewValue != null){
                    if(h.OldValue != null){
                        listToReturn.add(new Row(h.Id, h.CreatedDate, h.CreatedBy.Name, 'Case History', (fieldsMap.containsKey(h.Field.toLowerCase()) ? fieldsMap.get(h.Field.toLowerCase()).getDescribe().getLabel() : h.Field) + ' changed from ' + h.OldValue + ' to ' + h.NewValue, '', caseMap.get(h.CaseId)));
                    } else {
                        listToReturn.add(new Row(h.Id, h.CreatedDate, h.CreatedBy.Name, 'Case History', (fieldsMap.containsKey(h.Field.toLowerCase()) ? fieldsMap.get(h.Field.toLowerCase()).getDescribe().getLabel() : h.Field) + ' changed to ' + h.NewValue, '', caseMap.get(h.CaseId)));
                    }
                } else if(h.OldValue != null){
                    listToReturn.add(new Row(h.Id, h.CreatedDate, h.CreatedBy.Name, 'Case History', 'Deleted ' + h.OldValue + ' in ' + (fieldsMap.containsKey(h.Field.toLowerCase()) ? fieldsMap.get(h.Field.toLowerCase()).getDescribe().getLabel() : h.Field), '', caseMap.get(h.CaseId)));
                } else if(h.Field == 'closed'){
                    listToReturn.add(new Row(h.Id, h.CreatedDate, h.CreatedBy.Name, 'Case History', 'Case Closed', '', caseMap.get(h.CaseId)));
                }
            }
        }
        List<Case> cases = [SELECT Id, CreatedDate, CreatedBy.Name FROM Case WHERE Id in:selectedCaseIds];
        for(Case c : cases){
            listToReturn.add(new Row(c.Id, c.CreatedDate, c.CreatedBy.Name, 'Case History', 'Case Created', '', caseMap.get(c.Id)));
        }
        listToReturn.sort();
        return listToReturn;
    }

    public PageReference backToCase(){
        return new ApexPages.StandardController(theCase).view();
    }

    public PageReference export(){ 
        PageReference p = Page.CaseTimelineExcel;
        p.getParameters().put('Id',caseId);
        return p;
    }

    public class Row implements Comparable{
        public Id rowId{get; set;}
        public DateTime theDate{get; set;}
        public String dateTimeString{get; set;}
        public String userName{get; set;}
        public String type{get; set;}
        public String subject{get; set;}
        public String description{get; set;}
        public Case theCase{get; set;}

        public Row(Id i, DateTime dt, String u, String t, String s, String de, Case c){
            rowId = i;
            theDate = dt;
            userName = u;
            type = t;
            subject = s;
            description = de;
            dateTimeString = dt.format('dd/MM/yyyy HH:mm');
            theCase = c;
        }

        public Row(Id i, Date da, String u, String t, String s, String de, Case c){
            rowId = i;
            theDate = da;
            userName = u;
            type = t;
            subject = s;
            description = de;
            dateTimeString = da.format() + ' 00:00';
            theCase = c;
        }

        public Integer compareTo(Object compareTo){
            Row compareToRow = (Row)compareTo;
            if(theDate == compareToRow.theDate) return 0;
            if(theDate > compareToRow.theDate) return 1;
            return -1;
        }
    }
}